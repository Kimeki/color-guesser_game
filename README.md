# Color Guesser
A simple game written in JavaScript about colors. You're given a color, and you have to write down the correct name.

**It can handle as much colors as needed,** everything required for each one is its hexadecimal code and its name. There are thousands of ways to get an hexadecimal code of a color, such as using color tools of lots of programs, or using web pages (such as [color-hex](http://www.color-hex.com/) or [this color picker](https://www.w3schools.com/colors/colors_picker.asp))

This little program is under GNU GPL v3 license, so that everyone can get it and modify it if wanted.

To play, click the download button and select "Download zip". Extract it, and inside the folder that it will generate, open cg.html with your web browser. Then you can set the game up and play it.