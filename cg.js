var color = [];
var nm = [];
var todo;

var score = 0;
var tries = 0;
var question;
var msg = ["Correct!", "Incorrect! It was "];
var stats_txt = ["Score: ", "Tries: ", "Average: "];

var s_color; //Selected color.
var last;

var hex = "ABCDEFabcdef"; //A list that contains all non-numeric posible values inside a hexadecimal number.

function IsInHex(l) {
	for (a = 0; a < hex.length; a++) {
		if (l === hex[a]) {
			return true;
		}
	}
	return false;
	//Better using this than indexOf()
}

function WriteIn(cont) {
	//Used to write the generated content to the screen.
	document.getElementById("cont").innerHTML = cont;
}

function Check() {
	var ans = document.getElementById("answer").value;
	
	if (ans == nm[s_color]) {
		alert(msg[0]);
		score++;
	} else {
		alert(msg[1] + nm[s_color]);
	}
	
	tries++;
	Game(false);
}

function GetFrom(loc) {
	return document.getElementById(loc).value;
}

function Game(CollectData) {
	if (CollectData) {
		question = GetFrom("question");
		msg[0] = GetFrom("correct");
		msg[1] = GetFrom("incorrect");
		stats_txt[0] = GetFrom("score");
		stats_txt[1] = GetFrom("tries");
		stats_txt[2] = GetFrom("average");
	}
	
	var same = true;
	
	//Avoiding repeating colors.
	
	while (same) {
		s_color = Math.floor(Math.random() * todo);
		same = (s_color == last);
	}
	
	last = s_color; //Better save that ASAP to use it later. When recalling this function.
	var avg = function(score, tries) {if (tries == 0) {return 0} else {return ((score / tries) * 100)}};
	
	cont = "<h1>" + question + "</h1><div style='width: 100px; height: 100px; background-color: #" + color[s_color] + ";'></div><p><input type='text' id='answer'><input type='button' onClick='Check();' value='Check'></p><p>" + stats_txt[0] + score.toString() + "<br>" + stats_txt[1] + tries.toString() + "<br>" + stats_txt[2] + avg(score, tries).toFixed(2) + "%</p>";
	WriteIn(cont);
}

function showIt() {
	var cont = "<table align='center'><tr><th>Color</th><th>Name</th></tr>"
	
	for (x = 0; x < color.length; x++) {
		cont = cont + "<tr><td><div style='height: 50px; width: 50px; background-color: #" + color[x] + ";'></td><td>" + nm[x] + "</td></tr>";
	}
	cont = cont + "</table><p align='center'>Enter question: <input type='text' id='question'><br>When it's correct, I say: <input type='text' id='correct' value='"+ msg[0] + "'><br>When it's not: <input type='text' id='incorrect' value='" + msg[1] + "'> + the correct color<br>Score: <input type='text' id='score' value='" + stats_txt[0] + "'><br>Tries: <input type='text' id='tries' value='" + stats_txt[1] + "'><br>Average: <input type='text' id='average' value='" + stats_txt[2] + "'></p><p align='center'><input type='button' onClick='prepare();' value='Back'><input type='button' onClick='Game(true);' value='Start'></p>";
	
	//Check everything before starting, apart from allowing to customize the strings that will appear in the game. Not everybody speaks English, after all.
	
	WriteIn(cont);
}

function take() {
	var Clength = document.getElementsByClassName("color").length;
	var Nlength = document.getElementsByClassName("nm").length;
	
	//Checking values
	
	if (Clength == Nlength) {
		for (x = 0; x < Clength; x++) {
			nm[x] = document.getElementsByClassName("nm")[x].value;
			color[x] = document.getElementsByClassName("color")[x].value;
		}
		
		for (x = 0; x < color.length; x++) {
			if (color[x].length != 6) {
				alert("Please introduce colors in hex values without # (ex: 123456)")
				return;
			}
			
			for (n = 0; n < color[x].length; n++) {
				if (!IsInHex(color[x][n]) && isNaN(color[x][n])) {
					alert("Please introduce colors in hex values without # (ex: 123456)")
					return;
				}
			}
		}
		showIt();
	}
}

function prepare() {
	if (todo == null) {
		if (!isNaN(GetFrom("num"))) {
			todo = GetFrom("num");
		} else {
			return;
		}
	} else {
		nv = prompt("New number of colors? (Empty or 0: don't change)");
		//Just in case you want to do use a different set of numbers than before or mistyped it.
		if (!isNaN(nv)) {
			if (!(nv == 0 || nv == null)) {
				todo = nv;
			}
		}
		//Always check that the received value is a number.
	}
	
	var cont = "<table align='center'><tr><th>No.</th><th>Color (hex)</th><th>Name</th></tr>";
	
	for (x = 0; x < todo; x++) {
		cont = cont + "<tr><td>" + x + "</td><td><input type='text' class='color'></td><td><input type='text' class='nm'></td></tr>";
	}
	cont = cont + "</table><p align='center'><input type='button' onClick='take();' value='Enter'></p>";
	WriteIn(cont);
}

document.write("<div id='cont'><h1>How many colors?</h1><input type='text' id='num'><br><input type='button' onClick='prepare();' value='Enter'></div>"); //WriteIn() wouldn't work here. Also, putting the div with id cont prepares the document to receive any content from WriteIn().

/*
* Coded by Kimeki in 2018
* 
* Hoping it's useful for both having kids entertained
* as well as a way to learn about content generated on the fly ;)
*
* Feel free to talk to me if you need help.
*/
